
## Allgemeines

Dieses Projekt entstand im Rahmen eines Seminars zum Thema Web-Programmierung an der FSU Jena. Es wurde eine Rest API entwickelt, welche die Möglichkeit bietet Notizen, bestehend aus Titel und Inhalt, auf einem Webserver zu speichern. Dabei wurde der Fokus auf Sicherheit gelegt und die API durch SSL, sowie Nutzerauthentifizierung abgesichert. 

Realisiert wurde das Projekt als Maven-Projekt mithilfe von Spring-Boot. 

Zu Demonstrationszwecken wurde die API auf einem heimischen Raspberry PI bereitgestellt, welcher unter folgender Adresse erreichbar ist:
[https://salabasti.spdns.eu:8443]() Zur SSL-Absicherung wurde lediglich ein **selbst signiertes Zertifikat** genutzt. Je nach Client muss dies evtl. gesondert behandelt werden. 


## API Übersicht

Die API unterstützt drei grundlegende Methoden:

* speichern neuer Notizen
* abrufen aller Notizen eines Benutzers
* abrufen einzelner Notizen eines Benutzers

Eine Notiz besteht dabei jeweils aus einer Zeichenkette zu Titel und Inhalt und wird immer als JSON-Objekt im HTTP-Body übermittelt. 

Die Informationen zum Nutzer werden hingegen immer als Request-Parameter übertragen. Diese bestehen aus Benutzer-Name und -Passwort. Ist der Nutzername beim Anlegen einer Notiz noch nicht bekannt, so wird intern ein neuer Nutzer, mit dem mitgeliefertem Passwort angelegt. Existiert schon ein Nutzer mit diesem Namen, so wird das entsprechende Passwort überprüft. Eine Notiz ist nur mit dem beim Anlegen übermittelten Informationen abrufbar. 


## API Dokumentation

### Einzelne Notizen anlegen oder alle Notizen eines Benutzers abrufen

URL: 	

	/messages
		
Methods: 

	GET, PUT
		
URL-Parameter: 

	Required: Your user credentials
			name=[String]		
			password=[String]
		example: 
			?password=pass0815&name=user1
			
Data-Parameter: 

	example: for creating new note (PUT)
			
			{
				"title": "hello",
				"content": "This is my note!"
			}


  			
### Einzelne Notizen abrufen

URL: 	

	/messages/{id}
		> id is the id of the note
		
Methods: 
		
	GET
		
URL-Parameter: 
		
	Required: Your user credentials
		name=[String]		
		password=[String]
	
	example: ?password=pass0815&name=user1
			
Data-Parameter: 
	
	example:
			
			{
				"id": 1323
				"title": "hello",
				"content": "This is my note!"
			}
		
	
## CURL Beispiele

Da der Server nur mit einem selbst signiertem Zertifikat abgesichert ist, muss hier die Option '-k' verwendet werden. Diese deaktiviert die Zertifikat-Verifikation von curl.

Neue Notiz unter Nutzer 'Bob' mit Passwort 'pass' anlegen:
	
	curl -H 'Content-Type: application/json' -X PUT -d '{"title":"hello", "content":"this is my note!"}' -k 'https://localhost:8443/messages?name=bob&password=pass'

Alle Notizen für den Nutzer 'bob' abhlen:
	
	curl -X GET -k 'https://localhost:8443/messages?name=bob&password=pass'
	
Notiz mit der ID 1001 abholen (vorrausgesetzt diese wurde von Nutzer 'bob' angelegt):
	
	curl -X GET -k 'https://localhost:8443/messages/1001?name=bob&password=pass'