package restproject;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/messages")
public class MessageRepository {
	
	// Create new Notes
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> addMessage(@RequestBody Message m, @RequestParam String name, @RequestParam String password) {
		User u;
		try {
			u = UserHandler.sharedInstance().getUser(name, password);
		} catch (UserNotFoundException e) {
			u = UserHandler.sharedInstance().createUser(name, password);
		} catch (InvalidPasswordException e) {
			return ResponseEntity.status(401).build();
		}
			
		MessageHandler.sharedInstance().addMessageForUser(m, u);	
		return ResponseEntity.ok(m);
	}
	

	// get all notes for one user
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getMessages(@RequestParam String name, @RequestParam String password) {
		User u;
		try {
			u = UserHandler.sharedInstance().getUser(name, password);
		} catch (InvalidPasswordException | UserNotFoundException e) {
			return ResponseEntity.status(401).build();
		}
		
		return ResponseEntity.ok(MessageHandler.sharedInstance().getMessagesForUser(u));
	}
	
	
	// get single note with specific id for specific user
	@RequestMapping(path = "/{messageID}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getMessage(@PathVariable String messageID, @RequestParam String name, @RequestParam("password") String pass) {
		User u;
		try {
			u = UserHandler.sharedInstance().getUser(name, pass);
		} catch (InvalidPasswordException | UserNotFoundException e) {
			return ResponseEntity.status(401).build();
		}
		
		Message m;
		try {
			m = MessageHandler.sharedInstance().getMessageWithId(Integer.valueOf(messageID).intValue());
		} catch (NumberFormatException | MessageNotFoundException e) {
			return ResponseEntity.status(404).build();
		}
		
		if (m.getUserID() != u.getID()) 
			return ResponseEntity.status(401).build();
		
		return ResponseEntity.ok(m);
	}
}
