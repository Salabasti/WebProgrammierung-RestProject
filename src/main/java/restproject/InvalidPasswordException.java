package restproject;

public class InvalidPasswordException extends Exception {
	
	public InvalidPasswordException(String message) {
		super(message);
	}
}
