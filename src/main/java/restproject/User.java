package restproject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {
	
	private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
	private int id;

	private String name;
	private String password;
	
	private List<Integer> messageIDs = new ArrayList<Integer>();
	
	
	public User(String name, String password) {
		this.id = ID_GENERATOR.incrementAndGet();
		
		this.name = name;
		this.password = password;
	}
	
	
	public int getID() {
		return this.id;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	
//	@JsonIgnore
	public String getPassword() {
		return this.password;
	}	
	
	
	public void addMessage(int id) {
		this.messageIDs.add(id);
	}
	
	
	public List<Integer> getMessageIDs() {
		return this.messageIDs;
	}
	
	
	public String toString() {
		return "'id' : " + this.id + "'name' : " + this.name + ", 'password' : " + this.password + "\n";
	}
}
