package restproject;

import java.util.ArrayList;
import java.util.List;

public class UserHandler {

	private List<User> users = new ArrayList<User>();
	
	
	public User getUser(String name, String pass) throws InvalidPasswordException, UserNotFoundException {
		for (User u : this.users) {
			if (u.getName().equals(name)) {
				if (u.getPassword().equals(pass)) {
					return u;
				} else {
					throw new InvalidPasswordException("Incorrect Password!");
				}
			}
		}
		
		throw new UserNotFoundException("User with name" + name + " not found!");
	}
	
	
	public User createUser(String name, String pass) {
		User newUser = new User(name, pass);
		this.users.add(newUser);
		return newUser;
	}
	
	
	private static UserHandler shared;
	public static synchronized UserHandler sharedInstance() {
		if (shared == null) {
			shared = new UserHandler();
		}
		return shared;
	}
}
