package restproject;

import java.util.ArrayList;
import java.util.List;

public class MessageHandler {
	
	private List<Message> messages = new ArrayList<Message>();
	
	
	public void addMessageForUser(Message m, User u) {
		u.addMessage(m.getID());
		m.setUserID(u.getID());
		this.messages.add(m);
	}
	
	
	public List<Message> getMessagesForUser(User u) {
		ArrayList<Message> tmp = new ArrayList<Message>();
		for (Message m : this.messages) {
			if (m.getUserID() == u.getID()) {
				tmp.add(m);
			}
		}
		
		return tmp;
	}
	
	
	public Message getMessageWithId(int id) throws MessageNotFoundException {
		for (Message m : this.messages) {
			if (id == m.getID()) return m;
		}
		throw new MessageNotFoundException("Message with id " + id + " not found!");
	}
	
	
	private static MessageHandler shared;
	public static synchronized MessageHandler sharedInstance() {
		if (shared == null) {
			shared = new MessageHandler();
		}
		return shared;
	}
}
