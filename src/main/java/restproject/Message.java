package restproject;

import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Message {
	private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
	private int id;

	private String title = "";
	private String content = "";

	private int userID;

	
	public Message() {
		this.id = ID_GENERATOR.incrementAndGet();
	}
	
	
	public int getID() {
		return this.id;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}


	public String getTitle() {
		return this.title;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getContent() {
		return this.content;
	}
  
	
	public void setUserID(int id) {
		this.userID = id;
	}
	
  
	@JsonIgnore
	public int getUserID() {
		return this.userID;
	}
	
	
	public String toString() {
		return "'id' : " + this.id + "'title' : " + this.title + ", 'content' : " + this.content + ", 'userID' : " + this.userID + "\n";
	}
}
